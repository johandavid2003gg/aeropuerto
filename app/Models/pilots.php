<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pilots extends Model
{
    use HasFactory;

    //Hace referencia a la tabla 'pilots'
    protected $table = "pilots";


    //Realacion uno a muchos(Inversa) entre pilots y bases.
    public function bases(){
        return $this -> belongsTo('App\Models\bases');
    }

    //Relacion uno a muchos entre pilots y flights.
    public function flights(){
        return $this -> hasMany('App\Models\flights', 'pilots_id');
    }

}
