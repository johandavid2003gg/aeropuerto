<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class planes extends Model
{
    use HasFactory;

    //Hace referencia a la tabla 'planes'.
    protected $table = "planes";

    //Reacion uno a muchos (inversa) entre planes y bases.
    public function bases(){
        return $this -> belongsTo('App\Models\bases');
    }

    //Relacion uno a muchos entre planes y vuelos.
    public function flights(){
        return $this -> hasMany('App\Models\flights', 'planes_id');
    }
}
