<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class flights extends Model
{
    use HasFactory;

    //Hace referencia a la tabla 'flights'
    protected $table = "flights";


    //Relacion uno a muchos(Inversa) entre fligths y pilots.
    public function pilots(){
        return $this->belongsTo('App\Models\pilots');
    }

    //Relacion uno a muchos(Inversa) entre flights y planes.
    public function planes(){
        return $this->belongsTo('App\Models\planes');
    }

    //Relacion muchos a muchos entre flights y members.
    public function members(){
        return $this -> belongsToMany('App\Models\members','members_has_flights','flights_id','members_id');
        }
        
}
