<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bases extends Model
{
    use HasFactory;

    //Hace referencia a la tabla 'bases'.    
    protected $table = "bases";


    //Relacion uno a muchos entre Bases y pilots.
    public function pilots(){
        return $this -> hasMany('App\Models\pilots', 'bases_id');
    }


    //Relacion uno a muchos entre Bases y planes.
    public function planes(){
        return $this -> hasMany('App\Models\planes', 'bases_id');
    }


    //Relacion uno a muchos entre Bases y members.
    public function members(){
        return $this -> hasMany('App\Models\members', 'bases_id');
    }
}
