<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class members extends Model
{
    use HasFactory;

    //Hace referencia a la tabla 'members'.
    protected $table = "members";


    //Relacion uno a muchos(Inversa) entre members y bases.
    public function bases(){
        return $this -> belongsTo('App\Models\bases');
    }

    //Relacion muchos a muchos entre members y fligths.
    public function flights(){
    return $this -> belongsToMany('App\Models\flights', 'members_has_flights','members_id','flights_id');
    }
}
