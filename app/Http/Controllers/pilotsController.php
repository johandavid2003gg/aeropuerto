<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\pilots;
class pilotsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Muestra la vista con el formulario de registro de un piloto.
     * 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $basePilotos = $request->basePilotos;
        return view('pilots.create',compact('basePilotos'));
    }

    /**
     * Lleva a cabo el proceso de registro de un piloto.
     * 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store($basePilotos ,Request $request)
    {
        $pilotos = new pilots();
        $pilotos -> name = $request ->input('name');
        $pilotos -> flight_time = $request -> input('flight_time');
        $pilotos -> code = $request -> input('code');
        $pilotos -> Bases_id = $basePilotos;
        $pilotos->save();

        return redirect()->route('bases.index')->with('status7','El piloto fue agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
        
    }

    /**
     * Retorna la vista con el formulario para actualizar los datos del piloto.
     * 
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pilots = pilots::find($id);
        return view('pilots.edit',compact('pilots'));
    }

    /**
     * Lleva a cabo la actualizacion de los datos del piloto.
     * 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pilots = pilots::findOrFail($id);

        $pilots -> name = $request -> input('name');
        $pilots -> flight_time = $request -> input('flight_time');
        $pilots -> code = $request -> input('code');
        //$pilots -> bases_id = $basePilotos;

        $pilots -> save();

        return redirect()->route('bases.show' , ['basis'=> $pilots->Bases_id])->with('status8','Los datos del piloto fueron actualizados correctamente');
    }

    /**
     * Elimina el piloto
     * 
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pilots = pilots::find($id);
        $pilots -> delete();
        return redirect()->route('bases.show' , ['basis'=> $pilots->Bases_id])->with('status9','El piloto fue eliminado correctamente');
    }
}
