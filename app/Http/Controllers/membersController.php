<?php

namespace App\Http\Controllers;

use App\Models\bases;
use App\Models\members;
use Illuminate\Http\Request;

class membersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Muestra la vista con el formulario para añadir un nuevl miembro
     * 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $baseMembers = $request->baseMembers;
        return view('members.create',compact('baseMembers'));
    }

    /**
     * Lleva a cabo el proceso de registro de los miembros
     * 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($baseMembers ,Request $request)
    {
        $members = new members();

        $members -> name = $request -> input('name');
        $members -> code = $request -> input('code');
        $members -> Bases_id = $baseMembers;
        $members -> save();

        return redirect() -> route('bases.index') -> with('status4','El miembro fue agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        
    }

    /**
     * Retorna la vista con el formulario para editar la informacion de los miembros
     * 
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $members = members::find($id);
        return view('members.edit',compact('members'));
    }

    /**
     * Realiza el proceso de actualizacion de los datos del miembro
     * 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $members = members::findOrFail($id);
        $members -> name = $request -> name;
        $members -> code = $request -> code;
        $members -> save();

        return redirect()->route('bases.show' , ['basis'=> $members->Bases_id])-> with('status5','Los datos del miembro fueron actualizados correctamente');
    }

    /**
     * LLeva a cabo la eliminacion del miembro
     * 
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $members = members::find($id);
        $members -> delete();
        return redirect() -> route('bases.show', ['basis'=> $members->Bases_id])-> with('status6','El miembro fue eliminado correctamente');
    }
}
