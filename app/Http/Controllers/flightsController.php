<?php

namespace App\Http\Controllers;

use App\Models\flights;
use App\Models\members;
use App\Models\pilots;
use App\Models\planes;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class flightsController extends Controller
{
    /**
     * Retorna la vista donde estan listados todos lo vuelos
     * 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $flights = flights::all();
        return view('flights.index',compact('flights'));
    }

    /**
     * Retorna la vista con el formulario de creacion de vuelos
     * 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pilots = pilots::all();
        $planes = planes::all();
        $members = members::all();
        $flights = flights::with('pilots','planes','members')->get();
        return view('flights.create',compact('pilots','planes','flights','members'));
    }

    /**
     * Almacena los vuelos en la base de datos
     * 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $flights = new flights();
        $members = $request -> input('members_id');

        $flights -> flight_number = $request -> input('flight_number');
        $flights -> flight_hour = $request -> input('flight_hour');
        $flights -> destiny = $request -> input('destiny');
        $flights -> set_time = $request -> input('set_time');
        $flights -> pilots_id = $request -> input('pilots_id');
        $flights -> planes_id = $request -> input('planes_id');
        
        // dd($members);
        $flights -> save();
        $flights->members()->sync($members);

        


        // foreach($members as $member){
        
        //     $flights -> members() -> sync($member);

        // }

        // $flights -> save();

        // $member -> flights() -> attach($flights);
        
           

        return redirect() -> route('bases.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
