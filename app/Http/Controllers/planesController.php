<?php

namespace App\Http\Controllers;
use App\Models\bases;
use App\Models\planes;
use Illuminate\Http\Request;

class planesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Muestra la vista con el formulario para registar un avion.
     * 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $basePlanes = $request->basePlanes;
        return view('planes.create',compact('basePlanes'));
    }

    /**
     * Realiza el proceso de registro de aviones
     * 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($basePlanes ,Request $request)
    {
        $planes = new planes();

        $planes -> code = $request -> input('code');
        $planes -> Bases_id = $basePlanes;
        $planes -> save();

        return redirect() ->route('bases.index')->with('status10','El avion fue agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $bases = bases::find($id);
        // return view('planes.create',compact('bases'));
    }

    /**
     * Retorna la vista con el formulario de actualizacion de datos
     * 
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $planes = planes::find($id);
        return view('planes.edit',compact('planes'));
    }

    /**
     * Realiza el proceso de actualizacion de los datos del avion
     * 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $planes = planes::findOrFail($id);
        $planes -> code = $request->input('code');
        $planes -> save();

        return redirect()->route('bases.show' , ['basis'=> $planes->Bases_id])->with('status11','Los datos del avion fueron actualizados correctamente');
    }

    /**
     * Lleva a cabo el proceso de eliminacion del avion
     * 
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $planes = planes::find($id);
        $planes -> delete();
        return redirect()->route('bases.show' , ['basis'=> $planes->Bases_id])->with('status12','El avion fue eliminado correctamente');
    }
}
