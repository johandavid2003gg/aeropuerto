<?php

namespace App\Http\Controllers;
use App\Models\bases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class basesController extends Controller
{
    /** 
     * Muestra la vista con el listado de las bases de datos
     * 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bases = bases::all();
        return view('bases.index',compact('bases'));
    }

    /**
     * Muestra la vista con el formulario para crear un base aerea
     * 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('bases.create');
    }

    /**
     * almacena las bases aereas en la base de datos
     * 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bases = new bases();

        $bases -> name = $request -> input('name');
        $bases -> save();

        return redirect() -> action([basesController::class , 'index'])-> with('status1','La base fue creada correctamente');
    }

    /**
     * Muestra la informacion relacionada a cada base aerea
     * 
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bases = bases::find($id);
        $bases -> pilots;
        $bases -> planes;
        $bases -> members;

        return view('bases.show',compact('bases'));
    }

    /**
     * muestra la vista para editar la informacion de la base aerea
     * 
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bases = bases::find($id);
        return view('bases.edit',compact('bases'));
    }

    /**
     * Realiza el proceso de actualizaciomn de datos de las bases aereas
     * 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bases = bases::findOrFail($id);

        $bases -> name = $request -> input('name');
        $bases -> save();

        return redirect() -> route('bases.index')-> with('status2','La base fue actualizada correctamente');
    }

    /**
     * Elimina los datos de la base aerea junto a esta
     * 
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $bases = bases::find($id);
        $bases -> pilots() -> delete();
        $bases -> planes() -> delete();
        $bases -> members() -> delete();
        $bases -> delete();

        return redirect() -> route('bases.index')-> with('status3','La base fue eliminada correctamente');
    }
}
