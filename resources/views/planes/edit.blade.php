@extends('bootstrap.layout')

@section('title', 'Aviones')

@section('content')

    <form class="form-group" method="POST" action="/planes/{{$planes->id}}" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div style="padding: 5%; width:50%; margin-left:25%;">
            <label class="col-form-label col-form-label-lg" style="margin-bottom: 10px;">
                <H1>Informacion del Avion</H1>
            </label>
            <div class="row">
                <label class="col-sm-4">Codigo</label>
                <div class="col">
                    <input type="text" name="code" class="form-control" placeholder="{{$planes ->code}}">
                    <small class="form-text text-muted">Ingrese el codigo del avion</small>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" style="margin-left: 86%; margin-top:40px">actualizar</button>
        </div>
    </form>

@endsection
