@extends('bootstrap.layout')

@section('title', 'Pilotos')

@section('content')

    <form class="form-group" method="POST" action="{{route('pilots.store',$basePilotos)}}" enctype="multipart/form-data">
        @csrf
        <div style="padding: 5%; width:50%; margin-left:25%;">
            <label class="col-form-label col-form-label-lg" style="margin-bottom: 10px;">
                <H1>Informacion del piloto</H1>
            </label>
            <div class="row">
                <label class="col-sm-4">Nombre</label>
                <div class="col">
                    <input type="text" name="name" class="form-control">
                    <small class="form-text text-muted">Ingrese el nombre del piloto</small>
                </div>
            </div>
            <br>
            <div class="row">
                <label class="col-sm-4">Horas de Vuelo</label>
                <div class="col">
                    <input type="text" name="flight_time" class="form-control">
                    <small class="form-text text-muted">Ingrese las horas de vuelo</small>
                </div>
            </div>
            <br>
            <div class="row">
                <label class="col-sm-4">Codigo de Piloto</label>
                <div class="col">
                    <input type="text" name="code" class="form-control" >
                    <small class="form-text text-muted">Ingrese el Codigo del Piloto</small>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" style="margin-left: 86%; margin-top:40px">Registrar</button>
        </div>
    </form>

@endsection