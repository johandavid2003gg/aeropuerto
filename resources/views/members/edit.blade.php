@extends('bootstrap.layout')

@section('title', 'Miembros')

@section('content')

<form class="form-group" method="POST" action="/members/{{$members ->id}}" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div style="padding: 5%; width:50%; margin-left:25%;">
        <label class="col-form-label col-form-label-lg" style="margin-bottom: 10px;">
            <H1>Informacion del Miembro</H1>
        </label>
        <div class="row">
            <label class="col-sm-4">Nombre</label>
            <div class="col">
                <input type="text" name="name" class="form-control" placeholder="{{$members ->name}}">
                <small class="form-text text-muted">Ingrese el nombre del miembro</small>
            </div>
        </div>
        <br>
        <div class="row">
            <label class="col-sm-4">Codigo de miembro</label>
            <div class="col">
                <input type="text" name="code" class="form-control" placeholder="{{$members ->code}}">
                <small class="form-text text-muted">Ingrese el Codigo del Miembro</small>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" style="margin-left: 86%; margin-top:40px">Actualizar</button>
    </div>
</form>

@endsection