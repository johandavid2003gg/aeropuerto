@extends('bootstrap.layout')

@section('title', 'Bases Aereas')

@section('content')

    <div style="padding:50px;" class="row">
        <div class="col">
            <label style="color: royalblue">
                <h3>Pilotos</h3>
            </label>
            <table class="table" style=" width:450px">
                <thead class="head-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Horas de Vuelo</th>
                        <th scope="col">Codigo</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                @if (count($bases->pilots) > 0)
                    @foreach ($bases->pilots as $pilot)
                        <tr>
                            <th scope="row">{{ $pilot->id }}</th>
                            <td>{{ $pilot->name }}</td>
                            <td>{{ $pilot->flight_time }}</td>
                            <td>{{ $pilot->code }}</td>
                            <td>
                                <a href="http://127.0.0.1:8000/pilots/{{$pilot ->id}}/edit" class="btn btn-primary" style="border: none; background: white;" ><img src="/images/curriculum.png" style="background: white; height: 20px; width: 20px;" title="Editar"></a>
                                <form action="{{route('pilots.destroy', $pilot -> id)}}" method="POST" class="form form-group">
                                    @method('DELETE')
                                    @csrf
                                <button class="btn btn-primary" style="border: none; background: white;" ><img src="/images/borrar.png" style="background: white; height: 20px; width: 20px;" title="Eliminar Piloto" onClick="return confirm('seguro de elinar esta base aerea?')" title="Eliminar"></button>
                                </form>                           
                             </td>
                        </tr>
                    @endforeach
                @else
                @endif

            </table>
        </div>

        <div class="col" style="margin-left: 90px">
            <label style="color: royalblue">
                <h3>Aviones</h3>
            </label>
            <table class="table" style="width: 250px;" >
                <thead class="head-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Codigo</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                @if (count($bases->planes) > 0)
                    @foreach ($bases->planes as $plane)
                        <tr>
                            <th scope="row">{{ $plane->id }}</th>
                            <td>{{ $plane->code }}</td>
                            <td>
                                <a href="http://127.0.0.1:8000/planes/{{$plane ->id}}/edit" class="btn btn-primary" style="border: none; background: white;" ><img src="/images/curriculum.png" style="background: white; height: 20px; width: 20px;" title="Editar"></a>
                                <form action="{{route('planes.destroy', $plane -> id)}}" method="POST" class="form form-group">
                                    @method('DELETE')
                                    @csrf
                                <button class="btn btn-primary" style="border: none; background: white;" ><img src="/images/borrar.png" style="background: white; height: 20px; width: 20px;" title="Eliminar Piloto" onClick="return confirm('seguro de elinar esta base aerea?')" title="Eliminar"></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                @endif

            </table>
        </div>

        <div class="col">
            <label style="color: royalblue">
                <h3>Miembros</h3>
            </label>
            <table class="table" style=" width:350px">
                <thead class="head-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Codigo</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                @if (count($bases->members) > 0)
                    @foreach ($bases->members as $member)
                        <tr>
                            <th scope="row">{{ $member->id }}</th>
                            <td>{{ $member->name }}</td>
                            <td>{{ $member->code }}</td>
                            <td>
                                <a href="http://127.0.0.1:8000/members/{{$member ->id}}/edit" class="btn btn-primary" style="border: none; background: white;" ><img src="/images/curriculum.png" style="background: white; height: 20px; width: 20px;" title="Editar"></a>
                                <form action="{{route('members.destroy', $member -> id)}}" method="POST" class="form form-group">
                                    @method('DELETE')
                                    @csrf
                                <button class="btn btn-primary" style="border: none; background: white;" ><img src="/images/borrar.png" style="background: white; height: 20px; width: 20px;" title="Eliminar Piloto" onClick="return confirm('seguro de elinar esta base aerea?')" title="Eliminar"></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                @endif

            </table>
        </div>
        <div style="margin-left:10%; width:300px;">
            @include('messages.tareas')
        </div>
    </div>

@endsection
