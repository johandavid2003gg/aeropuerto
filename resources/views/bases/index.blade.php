@extends('bootstrap.layout')

@section('title', 'Bases Aereas')

@section('content')

    <div style="margin-left:10%; width:300px;">
        @include('messages.tareas')
    </div>
    <div style="margin_right:30px; margin-top:10px; margin-left: 35%">

        <label style="color: royalblue">
            <h1 style="margin-left: 40px">Lista de bases Aereas</h1>
            <br>

        </label>
        <table class="table" style="width: 500px">
            <thead class="head-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" >Nombres</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($bases as $base)
                    <tr>
                        <th scope="row">{{ $base->id }}</th>
                        <td>{{ $base->name }}</td>
                        <td><a href="http://127.0.0.1:8000/pilots/create?basePilotos={{ $base->id }}"
                                class="btn btn-primary" style="border: none; background: white;"><img
                                    src="/images/piloto.png" style="background: white; height: 20px; width: 20px;"
                                    title="Agregar Piloto"></a>

                            <a href="http://127.0.0.1:8000/planes/create?basePlanes={{ $base->id }}"
                                class="btn btn-primary" style="border: none; background: white;"><img
                                    src="/images/avion.png" style="background: white; height: 20px; width: 20px;"
                                    title="Agregar Avion"></a>

                            <a href="http://127.0.0.1:8000/members/create?baseMembers={{ $base->id }}"
                                class="btn btn-primary" style="border: none; background: white;"><img
                                    src="/images/personal.png" style="background: white; height: 20px; width: 20px;"
                                    title="Agregar Mienbros de la tripulacion"></a>

                            <a href="bases/{{ $base->id }}" class="btn btn-primary"
                                style="border: none; background: white;"><img src="/images/aerolinea.png"
                                    style="background: white; height: 20px; width: 20px;"
                                    title="Ver todo sobre la base"></a>

                            <a href="http://127.0.0.1:8000/bases/{{ $base->id }}/edit" class="btn btn-primary"
                                style="border: none; background: white;"><img src="/images/curriculum.png"
                                    style="background: white; height: 20px; width: 20px;" title="Editar"></a>

                            <form action="{{ route('bases.destroy', $base->id) }}" method="POST" class="form form-group">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-primary" style="border: none; background: white;"><img
                                        src="/images/borrar.png" style="background: white; height: 20px; width: 20px;"
                                        onClick="return confirm('seguro de elinar esta base aerea?')"
                                        title="Eliminar"></button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
        <div class="row" style="margin-left: 6%">
            <div class="col">
                <a href="http://127.0.0.1:8000/flights/create" class="btn btn-primary" style="margin-top: 10%">Asignar
                    Vuelos</a>
                <a href="http://127.0.0.1:8000/flights" class="btn btn-primary" style="margin-top: 10%">Ver vuelos
                    asignados</a>
            </div>
        </div>

    </div>

@endsection
