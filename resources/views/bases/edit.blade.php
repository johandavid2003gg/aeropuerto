@extends('bootstrap.layout')

@section('title', 'Bases Aereas')

@section('content')


    <form class="form-group" method="POST" action="/bases/{{$bases -> id}}" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div style="padding: 30px; margin-right:50%; ">
            <label class="col-form-label col-form-label-lg" style="margin-bottom: 10px;">
                <H1>Nombre de la base</H1>
            </label>
            <div class="row">
                <label class="col-sm-4">Nombre</label>
                <div class="col">
                    <input type="text" name="name" class="form-control" placeholder="{{$bases -> name}}">
                    <small class="form-text text-muted">Nuevo Nombre de la base.</small>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" style="margin-left: 86%; margin-top:40px">Actualizar</button>
        </div>
    </form>

@endsection