@extends('bootstrap.layout')

@section('title', 'Vuelo Destinados')

@section('content')

    <div style="padding: 30px; margin-left: 30%">
        <label style="color: royalblue">
            <h1>Lista de los vuelos asignados</h1>
            <hr class="my-4">
        </label>
        <table class="table" style="width: 500px">
            <thead class="head-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Numero de Vuelo</th>
                    <th scope="col">Hora de vuelo</th>
                    <th scope="col">destino</th>
                    <th scope="col">Duracion del vuelo</th>
                    <th scope="col">Piloto</th>
                    <th scope="col">Avion</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($flights as $flight)
                    <tr>
                        <th scope="row">{{ $flight->id }}</th>
                        <td>{{ $flight->flight_number }}</td>
                        <td>{{ $flight->flight_hour }}</td>
                        <td>{{ $flight->destiny }}</td>
                        <td>{{ $flight->set_time }}</td>
                        <td>{{ $flight->pilots_id }}</td>
                        <td>{{ $flight->planes_id }}</td>
                    </tr>
                @endforeach

            </tbody>
        </table>

    @endsection    