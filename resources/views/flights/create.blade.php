@extends('bootstrap.layout')

@section('title', 'Bases Aereas')

@section('content')

<form class="form-group" method="POST" action="{{route('flights.store')}}" enctype="multipart/form-data">
    @csrf
    <div style="padding: 5%; width:50%; margin-left:25%;">
        <label class="col-form-label col-form-label-lg" style="margin-bottom: 10px;">
            <H1>Asignacion de vuelos</H1>
        </label>
        <div class="row">
            <label class="col-sm-4">Numero del vuelo</label>
            <div class="col">
                <input type="text" name="flight_number" class="form-control">
                <small class="form-text text-muted">Asigne el numero del vuelo</small>
            </div>
        </div>
        <br>
        <div class="row">
            <label class="col-sm-4">Hora de vuelo</label>
            <div class="col">
                <input type="time" name="flight_hour" class="form-control">
                <small class="form-text text-muted">Asigne la hora de vuelo</small>
            </div>
        </div>
        <br>
        <div class="row">
            <label class="col-sm-4">Destino</label>
            <div class="col">
                <input type="text" name="destiny" class="form-control">
                <small class="form-text text-muted">Asigne el destino del vuelo</small>
            </div>
        </div>
        <br>
        <div class="row">
            <label class="col-sm-4">Duracion del vuelo</label>
            <div class="col">
                <input type="text" name="set_time" class="form-control">
                <small class="form-text text-muted">Asigne la duracion del vuelo</small>
            </div>
        </div>
        <br>
        <div class="row">
            <label class="col-sm-4">Piloto que realizara el vuelo</label>
            <div class="col">
                <select class="custom-select col-sm-12" name="pilots_id" style="height: 37px">
                    @foreach ($pilots as $pilot)
                    <option hidden></option>
                    <option value="{{$pilot -> id}}">{{$pilot -> name}}, {{$pilot -> code}}</option>
                    @endforeach
                </select>
                <small class="form-text text-muted">Asigne el piloto</small>
            </div>
        </div>
        <br>
        <div class="row">
            <label class="col-sm-4">Avion que realizara el vuelo</label>
            <div class="col">
                <select class="custom-select col-sm-12" name="planes_id" style="height: 37px">
                    @foreach ($planes as $plane)
                    <option hidden></option>
                    <option value="{{$plane -> id}}">{{$plane -> code}}</option>
                    @endforeach
                </select>
                <small class="form-text text-muted">Asigne el avion</small>
            </div>
        </div>

        <div class="row">
            <label class="col-sm-4">Miembros que estaran en el vuelo</label>
            <div class="col">
                <select multiple class="custom-select col-sm-12" name="members_id[]" >
                    @foreach ($members as $member)
                    <option hidden></option>
                    <option value="{{$member -> id}}">{{$member -> name}}</option>
                    @endforeach
                </select>
                <small class="form-text text-muted">Asigne el avion</small>
            </div>
        </div>

        {{-- <div class="row" hidden>
            <div class="col">
                <input type="text" name="bases_id" class="form-control" value="{{$flights -> id}}">
            </div>
        </div> --}}
        <button type="submit" class="btn btn-primary" style="margin-left: 86%; margin-top:40px">Registrar</button>
    </div>
</form>

@endsection