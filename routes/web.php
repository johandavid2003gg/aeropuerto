<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('bases', basesController::class);

Route::resource('pilots', pilotsController::class);

Route::resource('planes', planesController::class);

Route::resource('members', membersController::class);

Route::resource('flights', flightsController::class);

Route::post('pilots/{basePilotos}', ['as' => 'pilots.store', 'uses' => 'pilotsController@store']);

//Route::put('pilots/{basePilotos}', ['as' => 'pilots.update', 'uses' => 'pilotsController@update']);

Route::post('planes/{basePlanes}', ['as' => 'planes.store', 'uses' => 'planesController@store']);

Route::post('members/{baseMembers}', ['as' => 'members.store', 'uses' => 'membersController@store']);